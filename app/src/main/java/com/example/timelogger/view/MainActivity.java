package com.example.timelogger.view;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.timelogger.R;
import com.example.timelogger.model.Project;
import com.example.timelogger.viewmodel.ProjectViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private ProjectViewModel projectViewModel;
    private LinearLayout projectContainer;
    private TextView counterTextField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        projectViewModel = ViewModelProviders.of(this).get(ProjectViewModel.class);
        projectViewModel.initializeWith(getApplication());
        projectContainer = findViewById(R.id.project_container);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddProjectDialogFragment dialog = new AddProjectDialogFragment();
                dialog.show(getSupportFragmentManager(), "addProject");
            }
        });

        initializeProjectButtons();
    }

    private void initializeProjectButtons() {
        for (Project p: projectViewModel.getProjects()) {
            addProjectButton(p);
        }
    }

    public void addProject(String name, String category) {
        Project newProject = new Project(name, category);
        projectViewModel.addProject(newProject);
        addProjectButton(newProject);
    }

    private void setFormatAttributesOfButton(Button button) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(16, 16, 16, 16);
        button.setLayoutParams(layoutParams);

        button.setAllCaps(false);
        button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        button.setTextColor(Color.WHITE);
    }

    private void setOnClickListener(Button button, Project project) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                projectViewModel.logTimeForProject(project);
            }
        });
    }

    private void setButtonToObserveProject(Button button, Project project) {
        Project projectDataSource = projectViewModel.getProject(project);

        assert(projectDataSource != null);

        projectDataSource.getTitle().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String string) {
                button.setText(string);
            }
        });
    }

    private void addProjectButton(Project project) {
        Button newButton = new Button(this);
        setFormatAttributesOfButton(newButton);
        setOnClickListener(newButton, project);
        setButtonToObserveProject(newButton, project);

        projectContainer.addView(newButton);
    }
}