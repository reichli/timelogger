package com.example.timelogger.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.timelogger.R;

public class AddProjectDialogFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        final View dialogContent = inflater.inflate(R.layout.fragment_add_dialog, null);

        builder
                .setView(dialogContent)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String name = ((EditText)dialogContent.findViewById(R.id.name)).getText().toString();
                        String category = ((EditText)dialogContent.findViewById(R.id.category)).getText().toString();
                        ((MainActivity)getActivity()).addProject(name, category);
                        AddProjectDialogFragment.this.getDialog().cancel();
                    }
                })
                .setNegativeButton(R.string.abort, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AddProjectDialogFragment.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }
}
