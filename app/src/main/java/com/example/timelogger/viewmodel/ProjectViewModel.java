package com.example.timelogger.viewmodel;

import android.app.Application;

import androidx.lifecycle.ViewModel;

import com.example.timelogger.model.Project;
import com.example.timelogger.model.Repository;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProjectViewModel extends ViewModel {

    private Repository repository;

    private Thread timerThread;
    private AtomicBoolean isTimerRunning;
    private Project activeProject;

    public ProjectViewModel() {
        timerThread = null;
        isTimerRunning = new AtomicBoolean(false);
        activeProject = null;
        //TODO: load all projects from repository and then make the available in the UI
    }

    public void initializeWith(Application application) {
        repository = new Repository(application);
    }

    public void addProject(Project project) {
        repository.addProject(project);
    }

    public Project getProject(Project project) {
        return repository.getProject(project);
    }

    public ArrayList<Project> getProjects() {
        return repository.getProjects();
    }

    public boolean isTimerRunning() {
        return isTimerRunning.get();
    }

    public void startTimerForProject(Project project) {
        isTimerRunning = new AtomicBoolean(true);
        activeProject = getProject(project);

        assert(activeProject != null);

        activeProject.setActive(true);
        activeProject.getTitle().postValue(activeProject.toString());

        Runnable backgroundTask = new Runnable() {
            @Override
            public void run() {
                startNewWorkingTimeFor(activeProject);

                while (isTimerRunning.get()) {
                    try {
                        Thread.sleep(1000);
                        activeProject.recalcTodaysWorkingTimes();
                        activeProject.getTitle().postValue(activeProject.toString());
                    } catch (InterruptedException e) {
                        e.printStackTrace(); //TODO: need to do something with the exception here?
                    }
                }

                finishCurrentWorkingTimeFor(activeProject);
            }
        };

        timerThread = new Thread(backgroundTask);
        timerThread.start();
    }

    private void startNewWorkingTimeFor(Project project) {
        repository.addWorkingTime(project);
    }

    private void finishCurrentWorkingTimeFor(Project project) {
        repository.updateWorkingTime(project);
    }

    public void stopTimer() {
        if (timerThread != null && timerThread.isAlive() & isTimerRunning()) {
            isTimerRunning.set(false);
            try {
                timerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace(); //TODO: need to do something with the exception here?
            }

            activeProject.setActive(false);
            activeProject.getTitle().postValue(activeProject.toString());
            activeProject = null;
            timerThread = null;
        }
    }

    public void logTimeForProject(Project project) {
        if (isTimerRunning()) {
            assert(activeProject != null);
            if (project.equals(activeProject)) {
                stopTimer();
            } else {
                stopTimer();
                startTimerForProject(project);
            }
        }
        else {
            startTimerForProject(project);
        }
    }
}
