package com.example.timelogger.model;

import android.app.Application;

import androidx.room.Room;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Repository {

    private ProjectDatabase database;
    private ProjectDao projectDao;
    private ArrayList<Project> projects;

    public Repository(Application application) {
        database = Room.databaseBuilder(
                application.getApplicationContext(),
                ProjectDatabase.class, "database-name").fallbackToDestructiveMigration().build();

        projectDao = database.projectDao();
        projects = new ArrayList<>();
        Thread loadThread = new Thread(() -> {
            projects.addAll(projectDao.getAllProjects().stream().map(Project::new).collect(Collectors.toList()));
        });
        loadThread.start();
        try {
            loadThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace(); //TODO: need to do something with the exception here?
        }
    }

    public void addProject(Project project) {
        //TODO: What if project already exists? Name or category should be distinct?
        //TODO: refactor to service maybe?
        new Thread(() -> projectDao.insertProject(project)).start();
        projects.add(project);
    }

    //TODO: refactor this method away - it looks awful...
    public Project getProject(Project project) {
        for (Project p : projects) {
            if (p.equals(project)) {
                return p;
            }
        }

        //TODO: if the project's not in the list, maybe need to load it from the DB

        return null; //TODO: Throw exception! We have to handle this case!
    }

    public ArrayList<Project> getProjects() {
        return projects;
    }

    public void addWorkingTime(Project project) {
        WorkingTime newTime = new WorkingTime(LocalDateTime.now());
        newTime.setProjectId(project.getProjectId());
        project.getTimes().add(newTime);
    }

    public void updateWorkingTime(Project project) {
        WorkingTime time = project.getTimes().get(project.getTimes().size() - 1);
        time.setEnd(LocalDateTime.now());
        new Thread(() -> projectDao.insertWorkingTime(time)).start();
    }
}
