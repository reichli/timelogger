package com.example.timelogger.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(version = 1, entities = {Project.class, WorkingTime.class})
@TypeConverters({RoomConverters.class})
abstract class ProjectDatabase extends RoomDatabase {
    public abstract ProjectDao projectDao();
}
