package com.example.timelogger.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ProjectDao {

    @Insert
    void insertProject(Project project);

    @Insert
    void insertWorkingTime(WorkingTime time);

    @Query("select * from project")
    List<ProjectWithWorkingTimes> getAllProjects();
}
