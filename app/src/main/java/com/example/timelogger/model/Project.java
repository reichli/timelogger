package com.example.timelogger.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

@Entity
public class Project {

    @PrimaryKey(autoGenerate =  true)
    private int projectId;
    private String name;
    private String category;

    @Ignore
    private boolean isActive;
    @Ignore
    private long todaysWorkingTimes;
    @Ignore
    private MutableLiveData<String> title;
    @Ignore
    private ArrayList<WorkingTime> times;

    public Project(String name, String category) {
        this.name = name;
        this.category = category;

        isActive = false;
        todaysWorkingTimes = 0;
        times = new ArrayList<>();
        title = new MutableLiveData<>(toString());
    }

    @Ignore
    public Project(ProjectWithWorkingTimes rawData) {
        this.projectId= rawData.project.projectId;
        this.name = rawData.project.name;
        this.category = rawData.project.category;
        times = new ArrayList<>(rawData.workingTimes);

        isActive = false;
        recalcTodaysWorkingTimes();
        title = new MutableLiveData<>(toString());
    }

    public int getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    private boolean areOnSameDay(LocalDateTime a, LocalDateTime b) {
        return a.getYear() == b.getYear() && a.getDayOfYear() == b.getDayOfYear();
    };

    public void recalcTodaysWorkingTimes() {
        todaysWorkingTimes = times.stream()
                .filter(w -> { return areOnSameDay(w.getStart(), LocalDateTime.now()); })
                .mapToLong(w -> { return w.getDuration(); })
                .sum();
    }

    @NonNull
    @Override
    public String toString() {
        return (isActive ? "[AKTIV] " : "") + name + " / " + category + ": " + LocalTime.ofSecondOfDay(todaysWorkingTimes);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        Project otherProject = (Project)obj;

        return name == otherProject.name && category == otherProject.category;
    }

    public MutableLiveData<String> getTitle() {
        return title;
    }

    public ArrayList<WorkingTime> getTimes() {
        return times;
    }
}
