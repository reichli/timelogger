package com.example.timelogger.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.time.Duration;
import java.time.LocalDateTime;

@Entity
public class WorkingTime {

    @PrimaryKey(autoGenerate = true)
    private int workingTimeId;
    private LocalDateTime start;
    private LocalDateTime end;

    @ForeignKey(entity = Project.class,
            parentColumns = "projectId",
            childColumns = "projectId")
    private int projectId;

    public WorkingTime(LocalDateTime start) {
        this.start = start;
        end = null;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public int getWorkingTimeId() {
        return workingTimeId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public void setWorkingTimeId(int workingTimeId) {
        this.workingTimeId = workingTimeId;
    }

    public long getDuration() {
        return Duration.between(start, end == null ? LocalDateTime.now() : end).getSeconds();
    }
}
