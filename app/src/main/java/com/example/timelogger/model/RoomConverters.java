package com.example.timelogger.model;

import androidx.room.TypeConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RoomConverters {
    private static DateTimeFormatter format = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    @TypeConverter
    public static LocalDateTime toLocalDateTime(String dateString) {
        return dateString.isEmpty() ? null : LocalDateTime.parse(dateString, format);
    }

    @TypeConverter
    public static String fromLocalDateTime(LocalDateTime instantOfTime) {
        return instantOfTime == null ? "" : instantOfTime.format(format);
    }
}
