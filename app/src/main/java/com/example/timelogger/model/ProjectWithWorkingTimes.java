package com.example.timelogger.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class ProjectWithWorkingTimes {

    @Embedded
    public Project project;

    @Relation(parentColumn = "projectId", entityColumn = "projectId")
    public List<WorkingTime> workingTimes;

    public ProjectWithWorkingTimes(Project project, List<WorkingTime> workingTimes) {
        this.project = project;
        this.workingTimes = workingTimes;
    }
}
